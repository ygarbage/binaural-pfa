#include <iostream>
#include <cstring>

/* Pr�vention contre un bogue de compilation */
#if defined(__BORLANDC__)
namespace std {
using ::__strcmp__;
}
#endif

#include "osc/OscReceivedElements.h"
#include "osc/OscPacketListener.h"
#include "ip/UdpSocket.h"

#define START "/start"
#define STOP "/stop"
#define LOAD "/load"
#define HRTF "/hrtf"
#define VOLUME "/volume"
#define POS "/pos"

#define PORT 3011

class ExamplePacketListener : public osc::OscPacketListener {
protected:

    virtual void ProcessMessage( const osc::ReceivedMessage& m, 
				const IpEndpointName& remoteEndpoint ) {
        (void) remoteEndpoint; /* Suppression d'un warning de param�tre inutilis� */

        try {
            /* D�paquetage des diff�rents messages re�us selon l'en-t�te de message */
            
            if ( std::strcmp( m.AddressPattern(), START ) == 0 ) {
                std::cout << "Reception du signal de depart : " << START << " \n";
            }
			else if (std::strcmp(m.AddressPattern(), STOP) == 0) {
				std::cout << "Reception du signal de fin : " << STOP << " \n";
			}
			else if (std::strcmp(m.AddressPattern(), LOAD) == 0) {
				osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
				const char * a1; /* Nom de la piste */
				args >> a1 >> osc::EndMessage;

				std::cout << "Reception du nom d'une piste (en-tete : " << LOAD << " ) : "
					<< a1 << " \n";
			}
			else if (std::strcmp(m.AddressPattern(), HRTF) == 0) {
				osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
				const char * a1; /* Nom de la HRTF */
				args >> a1 >> osc::EndMessage;

				std::cout << "Reception du nom d'une HRTF (en-tete : " << HRTF << " ) : "
					<< a1 << " \n";
			}
			else if (std::strcmp(m.AddressPattern(), VOLUME) == 0) {
				osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
				float a1; /* Volume */
				args >> a1 >> osc::EndMessage;

				std::cout << "Reception du volume (en-tete : " << VOLUME << " ) : "
					<< a1 << "\n";
			}
			else if (std::strcmp(m.AddressPattern(), POS) == 0) {
				osc::ReceivedMessageArgumentStream args = m.ArgumentStream();
				float lat, lon, pro;
				args >> lat >> lon >> pro >> osc::EndMessage;

				std::cout << "Reception de positions (en-tete : )" << POS << " ) : "
					<< lat << ", " << lon << ", " << pro << " \n";
			}
        }
		catch ( osc::Exception& e ) {
			/* Toutes les erreurs de d�paquetage (types d'arguments inattendus ou arguments manquants) l�vent des exceptions */
            std::cout << "Erreur pendant la reception du message : "
                << m.AddressPattern() << " : " << e.what() << " \n";
        }
    }
};

int main(int argc, char* argv[])
{
    (void) argc; /* Suppression d'un warning de param�tre inutilis� */
    (void) argv; /* Suppression d'un warning de param�tre inutilis� */

    ExamplePacketListener listener;
	/* Initialiser un socket (interface de connexion) */
    UdpListeningReceiveSocket socket(
            IpEndpointName( IpEndpointName::ANY_ADDRESS, PORT ),
            &listener );

    std::cout << "Tests de reception OSC - ecoute sur le port " << PORT << " - (C-c) pour cesser\n";

	/* Lancer l'�coute, jusqu'� interruption par (C-c) */
    socket.RunUntilSigInt();

	std::cout << "Fin des tests de reception OSC\n";

    return 0;
}
