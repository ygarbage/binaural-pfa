#include "../osc/OscOutboundPacketStream.h"
#include "../ip/UdpSocket.h"

#include "sender.h"

extern "C"
{
	/*
	 *	Envoie le signal de d�part
	 */
	__declspec(dllexport) int osc_sendStart() {

		/* Initialiser un socket (interface de connexion) */
		UdpTransmitSocket socket(IpEndpointName(ADDRESS, START_PORT));
		char buffer[OUTPUT_BUFFER_SIZE];
		/* Initialiser un paquet OSC */
		osc::OutboundPacketStream packet(buffer, OUTPUT_BUFFER_SIZE);

		/* Empaqueter le signal de d�part */
		packet << osc::BeginBundleImmediate
			<< osc::BeginMessage(START)
			<< osc::EndMessage
			<< osc::EndBundle;

		/* Envoyer le paquet */
		socket.Send(packet.Data(), packet.Size());

		return SEND_SUCCESS; /* Indiquer que l'envoi a eu lieu */
	}

	/*
	 *	Envoie le signal de fin
	 */
	__declspec(dllexport) int osc_sendStop() {

		/* Initialiser un socket (interface de connexion) */
		UdpTransmitSocket socket(IpEndpointName(ADDRESS, STOP_PORT));
		char buffer[OUTPUT_BUFFER_SIZE];
		/* Initialiser un paquet OSC */
		osc::OutboundPacketStream packet(buffer, OUTPUT_BUFFER_SIZE);

		/* Empaqueter le signal de fin */
		packet << osc::BeginBundleImmediate
			<< osc::BeginMessage(STOP)
			<< osc::EndMessage
			<< osc::EndBundle;

		/* Envoyer le paquet */
		socket.Send(packet.Data(), packet.Size());

		return SEND_SUCCESS; /* Indiquer que l'envoi a eu lieu */
	}

	/*
	 * Envoie la cha�ne de caract�res <audio> re�ue en param�tre
	 */
	__declspec(dllexport) int osc_sendLoad(const char * audio) {

		/* Initialiser un socket (interface de connexion) */
		UdpTransmitSocket socket(IpEndpointName(ADDRESS, LOAD_PORT));
		char buffer[OUTPUT_BUFFER_SIZE];
		/* Initialiser un paquet OSC */
		osc::OutboundPacketStream packet(buffer, OUTPUT_BUFFER_SIZE);

		/* Empaqueter le nom de la piste � envoyer (cha�ne de caract�res audio) */
		packet << osc::BeginBundleImmediate
			<< osc::BeginMessage(LOAD)
			<< audio
			<< osc::EndMessage
			<< osc::EndBundle;

		/* Envoyer le paquet */
		socket.Send(packet.Data(), packet.Size());

		return SEND_SUCCESS; /* Indiquer que l'envoi a eu lieu */
	}

	/*
	 * Envoie la cha�ne de caract�res <hrtf> re�ue en param�tre
	 */
	__declspec(dllexport) int osc_sendHRTF(const char * hrtf) {

		/* Initialiser un socket (interface de connexion) */
		UdpTransmitSocket socket(IpEndpointName(ADDRESS, HRTF_PORT));
		char buffer[OUTPUT_BUFFER_SIZE];
		/* Initialiser un paquet OSC */
		osc::OutboundPacketStream packet(buffer, OUTPUT_BUFFER_SIZE);

		/* Empaqueter le nom de la HRTF � envoyer (cha�ne de caract�res hrtf) */
		packet << osc::BeginBundleImmediate
			<< osc::BeginMessage(HRTF)
			<< hrtf
			<< osc::EndMessage
			<< osc::EndBundle;

		/* Envoyer le paquet */
		socket.Send(packet.Data(), packet.Size());

		return SEND_SUCCESS; /* Indiquer que l'envoi a eu lieu */
	}

	/*
	 *	Envoie le volume <v> re�u en param�tre
	 */
	__declspec(dllexport) int osc_sendVolume(float v) {

		/* Initialiser un socket (interface de connexion) */
		UdpTransmitSocket socket(IpEndpointName(ADDRESS, VOLUME_PORT));
		char buffer[OUTPUT_BUFFER_SIZE];
		/* Initialiser un paquet OSC */
		osc::OutboundPacketStream packet(buffer, OUTPUT_BUFFER_SIZE);

		/* Empaqueter la valeur du volume � envoyer (v) */
		packet << osc::BeginBundleImmediate
			<< osc::BeginMessage(VOLUME)
			<< (float)v
			<< osc::EndMessage
			<< osc::EndBundle;

		/* Envoyer le paquet */
		socket.Send(packet.Data(), packet.Size());

		return SEND_SUCCESS; /* Indiquer que l'envoi a eu lieu */
	}

	/*
	*	Envoie les nombres <lat>, <lon> et <pro> re�us en param�tre
	*/
	__declspec(dllexport) int osc_sendPos(float lat, float lon, float pro) {

		/* Initialiser un socket (interface de connexion) */
		UdpTransmitSocket socket(IpEndpointName(ADDRESS, POS_PORT));
		char buffer[OUTPUT_BUFFER_SIZE];
		/* Initialiser un paquet OSC */
		osc::OutboundPacketStream packet(buffer, OUTPUT_BUFFER_SIZE);

		/* Empaqueter les trois valeurs (latitude, longitude, profondeur) � envoyer */
		packet << osc::BeginBundleImmediate
			<< osc::BeginMessage(POS)
			<< (float)lat
			<< (float)lon
			<< (float)pro
			<< osc::EndMessage
			<< osc::EndBundle;

		/* Envoyer le paquet */
		socket.Send(packet.Data(), packet.Size());

		return SEND_SUCCESS; /* Indiquer que l'envoi a eu lieu */
	}
}
