#define LOAD_PORT 3011
#define START_PORT 3011
#define STOP_PORT 3011
#define VOLUME_PORT 3011
#define HRTF_PORT 3011
#define POS_PORT 3011

#define OUTPUT_BUFFER_SIZE 1024
#define ADDRESS "127.0.0.1"

#define START "/start"
#define STOP "/stop"
#define LOAD "/load"
#define HRTF "/hrtf"
#define VOLUME "/volume"
#define POS "/pos"

#define SEND_SUCCESS 0
