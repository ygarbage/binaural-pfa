Installation
** Pr�requis **

	Les logiciels et librairies suivants sont n�cessaires au bon fonctionnement du projet.

* Kinect SDK
	Vous pouvez r�cup�rer le SDK (Kit de D�veloppement) de Kinect � cette adresse : http://www.microsoft.com/en-us/kinectforwindowsdev/Downloads.aspx

* Visual Studio
	Vous pouvez r�cup�rer Visual Studio Express (version sans pr�requis) pour Windows � cette adresse : http://www.microsoft.com/en-us/download/details.aspx?id=40787

* OpenCV
	La derni�re version d�OpenCV peut-�tre obtenue sur http://opencv.org/downloads.html. Au cas o� la derni�re version d�OpenCV ne vous convienne pas, sachez que le projet a �t� r�alis� avec la version 2.4.8 d�OpenCV. Installez OpenCV � la racine de votre disque C:/ et les liens devraient fonctionner avec VisualStudio.
	Il faut �galement installer emgu, avec la derni�re version qui n'est pas en b�ta : http://sourceforge.net/projects/emgucv/files/emgucv/ 

* Pure Data
	Une version de Pure Data Extended est n�cessaire pour ce projet, vous pouvez t�l�charger ce logiciel � l�adresse : http://puredata.info/downloads/pd-extended. Une fois le logiciel t�l�charg�, installez-le dans votre r�pertoire favori. Vous n�aurez rien � configurer par la suite dans ce logiciel.

* R�cup�ration du projet GIT
	La version la plus r�cente du projet peut �tre r�cup�r�e � l�adresse suivante : https://bitbucket.org/ygarbage/binaural-pfa

* Divers
	Une balle jaune vous sera n�cessaire pour que la d�tection de l�objet puisse se faire (ou toute chose ressemblante : citron, etc.) Une Kinect est �galement indispensable.

** Lancement du logiciel

	Pour utiliser notre solution, il vous suffit dans un premier temps de lancer le fichier PureData �main.pd� situ� dans le dossier Pure_data, puis de compiler et ex�cuter le fichier de projet Visual Studio (.sln) dans le dossier Menu_audio, afin d�avoir acc�s au corps principal du logiciel.
Veuillez noter qu�il serait bon de brancher la Kinect en premier lieu, afin de diminuer le temps de chargement initial.

Les pistes audios � utiliser pour l�exp�rience d��coute doivent �tre plac�es dans le dossier sons de l�application. Veillez � ce qu�aucun accent ne soit pr�sent dans les noms des pistes audios. En revanche, les espaces et les chiffres ne sont pas g�nants.