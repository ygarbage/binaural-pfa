﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;
using Microsoft.Kinect;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.IO;
using Microsoft.Win32;
using System;
using System.Drawing;
using System.Windows.Media;


namespace Detection_Objet_Namespace
{
    public class detObjClass
    {
        public static float[] DetectionObjet(WriteableBitmap colorBitmapParam)
        {

            float[] x_y_z = new float[3];
            //Pour detecter la teinte jaune
            Emgu.CV.Structure.MCvScalar hsv_min = new MCvScalar(10, 145, 145);
            Emgu.CV.Structure.MCvScalar hsv_max = new MCvScalar(90, 255, 255);

            //conversion de la writeable passée en paramètre en bitmap afin de générer une image emgucv valide
            System.Drawing.Bitmap bmp;
            using (MemoryStream outStream = new MemoryStream())
            {
                
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create((BitmapSource)colorBitmapParam));
                enc.Save(outStream);
                bmp = new System.Drawing.Bitmap(outStream);
            }
            

            Image<Bgr, byte> frame = new Image<Bgr, byte>(bmp);


            Image<Hsv, byte> hsv_frame = new Image<Hsv, byte>(640, 480);
            Image<Gray, byte> thresholded = new Image<Gray, byte>(640, 480);



            hsv_frame = frame.Convert<Hsv, byte>();


            // Filter out colors which are out of range.
            CvInvoke.cvInRangeS(hsv_frame.Ptr, hsv_min, hsv_max, thresholded.Ptr);
            // Memory for hough circles
            IntPtr storage = CvInvoke.cvCreateMemStorage(0);
            // hough detector works better with some smoothing of the image
            CvInvoke.cvSmooth(thresholded.Ptr, thresholded.Ptr, Emgu.CV.CvEnum.SMOOTH_TYPE.CV_GAUSSIAN, 9, 9, 0, 0);


            Gray cannyThreshold = new Gray(100);
            Gray cannyThresholdLinking = new Gray(50);


            CircleF[][] circles = thresholded.HoughCircles(cannyThreshold, cannyThresholdLinking, 2, thresholded.Height / 4, 5, 400);

            foreach (CircleF circle in circles[0])
            {
                //Là j'imagine que on envoie le protocole OSC.
                //En plus là j'ai que cette saloperie de X et Y il me faut aussi la profondeur du pixel x,y
                //mais Yvon peut être que tu sais comment faire ?
                Console.WriteLine("Balle x:%d  y : %d \n", circle.Center.X, circle.Center.Y);
                x_y_z[0] = circle.Center.X;
                x_y_z[1] = circle.Center.Y;
                frame.Draw(circle, new Bgr(System.Drawing.Color.Red), 2);//Ca dessine juste un rond rouge autour de la balle.
            }
            return x_y_z;
        }
    }
}
