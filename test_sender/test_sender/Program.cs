﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace test_sender
{
    class Program
    {
        /* Début des déclarations des fonctions implémentées dans la dll communication.dll */
        [DllImport("communication.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int osc_sendStart();
        [DllImport("communication.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int osc_sendStop();
        [DllImport("communication.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int osc_sendLoad(String audio);
        [DllImport("communication.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int osc_sendHRTF(String hrtf);
        [DllImport("communication.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int osc_sendVolume(float v);
        [DllImport("communication.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int osc_sendPos(float lat, float lon, float pro);
        /* Fin des déclarations des fonctions implémentées dans la dll communication.dll */

        static void Main(string[] args) {

            int status; /* Pour stocker la valeur retournée par la dll lors des appels de fonctions */

            Console.WriteLine("Tests d'envoi OSC en utilisant la dll de communication - envoi sur le port 3011");

            Console.WriteLine("\nEnvoi du signal de départ.. ");
            status = osc_sendStart();
            Console.WriteLine("valeur retournée par la DLL : {0}", status);

            Console.WriteLine("\nEnvoi du signal de fin.. ");
            status = osc_sendStop();
            Console.WriteLine("valeur retournée par la DLL : {0}", status);

            Console.WriteLine("\nEnvoi du nom d'une piste (1) : abcdefgh.wav");
            status = osc_sendLoad("abcdefgh.wav");
            Console.WriteLine("valeur retournée par la DLL : {0}", status);
            Console.WriteLine("\nEnvoi du nom d'une piste (2) : abc def gh.wav");
            status = osc_sendLoad("abc def gh.wav"); /* Nom contenant des espaces */
            Console.WriteLine("valeur retournée par la DLL : {0}", status);
            Console.WriteLine("\nEnvoi du nom d'une piste (3) : piste123.wav"); /* Nom contenant des chiffres */
            status = osc_sendLoad("piste123.wav");
            Console.WriteLine("valeur retournée par la DLL : {0}", status);
            Console.WriteLine("\nEnvoi du nom d'une piste (4) : piste stockée dans une variable.test"); /* Nom stocké dans une variable */
            String audio = "piste stockée dans une variable.test";
            status = osc_sendLoad(audio);
            Console.WriteLine("valeur retournée par la DLL : {0}", status);

            Console.WriteLine("\nEnvoi du nom d'une HRTF (1) : hrtf");
            status = osc_sendHRTF("hrtf");
            Console.WriteLine("valeur retournée par la DLL : {0}", status);
            Console.WriteLine("\nEnvoi du nom d'une HRTF (2) : hrtf abc def");
            status = osc_sendHRTF("hrtf abc def"); /* Nom contenant des espaces */
            Console.WriteLine("valeur retournée par la DLL : {0}", status);
            Console.WriteLine("\nEnvoi du nom d'une HRTF (3) : hrtf123.wav"); /* Nom contenant des chiffres */
            status = osc_sendHRTF("hrtf123.wav");
            Console.WriteLine("valeur retournée par la DLL : {0}", status);
            Console.WriteLine("\nEnvoi du nom d'une HRTF (4) : hrtf stockée dans une variable.test"); /* Nom stocké dans une variable */
            String hrtf = "hrtf stockée dans une variable.test";
            status = osc_sendHRTF(hrtf);
            Console.WriteLine("valeur retournée par la DLL : {0}", status);

            Console.WriteLine("\nEnvoi du volume (1) : 0"); /* Cas limite : volume = 0 */
            status = osc_sendVolume((float)0);
            Console.WriteLine("valeur retournée par la DLL : {0}", status);
            Console.WriteLine("\nEnvoi du volume (2): 0.75"); /* Cas normal : volume entre 0 et 1 */
            status = osc_sendVolume((float)0.75);
            Console.WriteLine("valeur retournée par la DLL : {0}", status);
            Console.WriteLine("\nEnvoi du volume (3): 1"); /* Cas limite : volume = 1 */
            status = osc_sendVolume((float)1);
            Console.WriteLine("Valeur renvoyée par la DLL : {0}", status);
            Console.WriteLine("\nEnvoi du volume (4): 0.20"); /* Volume stocké dans une variable */
            float v = (float)0.20;
            status = osc_sendVolume(v);
            Console.WriteLine("Valeur renvoyée par la DLL : {0}", status);

            Console.WriteLine("\nEnvoi de positions (1) : 0 0 0"); /* Cas limite : tout à 0 */
            status = osc_sendPos((float)0, (float)0, (float)0);
            Console.WriteLine("valeur retournée par la DLL : {0}", status);

            Console.WriteLine("\nEnvoi de positions (2) : 1.234567, -0.456789, 10.432108"); /* Cas normal : valeurs flotantes */
            status = osc_sendPos((float)1.234567, (float)-0.456789, (float)10.432108);
            Console.WriteLine("valeur retournée par la DLL : {0}", status);

            Console.WriteLine("\nFin des tests d'envoi OSC");
            Console.ReadLine();
        }
    }
}
