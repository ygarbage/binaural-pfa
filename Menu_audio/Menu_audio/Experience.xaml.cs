﻿using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Menu_audio
{
    /// <summary>
    /// Logique d'interaction pour Experience.xaml
    /// </summary>
    public partial class Experience : Page
    {
        private Utils utils = new Utils();

        /* Chargement dynamique des dll du module OSC */
        [DllImport("Envoi_osc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int osc_sendLoad(String audio);
        [DllImport("Envoi_osc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int osc_sendVolume(float v);

        [DllImport("Envoi_osc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int osc_sendPos(float v, float y, float z);
        [DllImport("Envoi_osc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int osc_sendStart();
        [DllImport("Envoi_osc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int osc_sendStop();
        /* Fin du chargement dynamique */

        private KinectSensor sensor;
        private readonly KinectSensorChooser sensorChooser;


        // Bitmap qui contiendra les informations sur la couleur  
        private WriteableBitmap colorBitmap;

        // Tableau pour le stockage des données de couleur reçues par la Kinect
        private byte[] colorPixels;

        // Coordonnées de la tête de l'utilisateur
        float headX, headY, headZ;

        // Eléments nécessaires au dessin du squelette
        private DrawingGroup drawingGroup;/* = new DrawingGroup();*/
        private const float RenderWidth = 640.0f;
        private const float RenderHeight = 480.0f;
        private const double JointThickness = 3;
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68))/*Brushes.Tomato*/;
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        private int SkeletonFramesCounter = 0;

        // Elément nécessaire à la conversion des coordonnées
        Spherique sph = new Spherique();

        // Variable pour la gestion des boutons start et stop
        private int stop = 0;

        public Experience()
        {
            // Initialisation de la Kinect
            this.InitializeComponent();
            this.sensorChooser = new KinectSensorChooser();
            // Réutilisation du sensorChooser de la première fenêtre
            this.sensorChooser = Generics.GlobalKinectSensorChooser;
            this.sensorChooser.KinectChanged += SensorChooserOnKinectChanged;
            this.sensorChooserUi.KinectSensorChooser = sensorChooser;
            // On relie le capteur Kinect à la KinectRegion définie dans le .xaml
            var regionSensorBinding = new Binding("Kinect") { Source = this.sensorChooser };
            BindingOperations.SetBinding(this.kinectRegion, KinectRegion.KinectSensorProperty, regionSensorBinding);

            // On cherche toutes les potentielles Kinects connectées
            foreach (var potentialSensor in KinectSensor.KinectSensors)
            {
                if ((potentialSensor.Status == KinectStatus.Connected) && (!potentialSensor.ColorStream.IsEnabled))
                {
                    this.sensor = potentialSensor;
                    break;
                }
            }

            // Si on a trouvé un capteur Kinect
            if (null != this.sensor)
            {
                // Activation du flux couleur
                this.sensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
                // Allocation d'espace pour les pixels reçus
                this.colorPixels = new byte[this.sensor.ColorStream.FramePixelDataLength];
                // Bitmap qui apparaît à l'écran
                this.colorBitmap = new WriteableBitmap(this.sensor.ColorStream.FrameWidth, this.sensor.ColorStream.FrameHeight, 96.0, 96.0, PixelFormats.Bgr32, null);
                this.Image.Source = this.colorBitmap;
                // Ajout d'un gestionnaire qui traite chaque nouveau paquet de couleurs reçu
                this.sensor.ColorFrameReady += this.SensorColorFrameReady;

                // Activation du flux squelette
                Console.WriteLine("activation flux squelette");
                this.sensor.SkeletonStream.Enable();
                // Ajout d'un gestionnaire qui traite chaque nouveau paquet squelette reçu
                this.sensor.SkeletonFrameReady += this.SensorSkeletonFrameReady;

                this.sensor.Start();

            }
            Loaded += OnLoaded;
        }

        // Gestionnaire de traitement d'une trame couleur
        private void SensorColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            // Ouverture de la trame couleur
            using (ColorImageFrame colorFrame = e.OpenColorImageFrame())
            {
                if (colorFrame != null)
                {
                    // On copie les données des pixels de l'image dans un tableau temporaire
                    colorFrame.CopyPixelDataTo(this.colorPixels);
                    // On les écrit dans notre Bitmap pour qu'ils soient affichés
                    this.colorBitmap.WritePixels(
                        new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight),
                        this.colorPixels,
                        this.colorBitmap.PixelWidth * sizeof(int),
                        0);
                }
            }
        }

        // Gestionnaire de traitement d'une trame de flux squelette (affichage du squelette non fonctionnel)
        private void SensorSkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            this.drawingGroup = new DrawingGroup();
            // Ajout d'un compteur pour n'afficher les positions de la tête que toutes les 10 trames reçues
            SkeletonFramesCounter += 1;
            if (SkeletonFramesCounter % 10 != 0)
            {
                return;
            }

            Skeleton[] skeletons = new Skeleton[6];

            // Ouverture des trames squelette reçues
            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame != null)
                {
                    Console.WriteLine("trame de {0} squelettes", skeletons.Length);
                    // On copie les données de la trame squelette dans un tableau temporaire
                    skeletonFrame.CopySkeletonDataTo(skeletons);
                }
            }

            using (DrawingContext dc = this.drawingGroup.Open())
            {

                if (skeletons.Length != 0)
                {
                    // Pour chaque squelette détecté
                    foreach (Skeleton skel in skeletons)
                    {

                        // Si le squelette est tracké
                        if (skel == null)
                            Console.WriteLine("Squelette nul");
                        else if (skel.TrackingState == SkeletonTrackingState.Tracked)
                        {
                            // On dessine le point correspondant à la tête
                            this.DrawBonesAndJoints(skel, dc);
                            Console.WriteLine("Squelette tracké");


                            DistanceHeadObject(skel, sph);
                            Console.WriteLine("lat : {0}, long : {1}, prof : {2}", sph.lat, sph.lon, sph.prof);


                            osc_sendPos(sph.lat, sph.lon, sph.prof);


                        }

                    }
                }

            }
        }

        private void DrawBonesAndJoints(Skeleton skeleton, DrawingContext drawingContext)
        {
            // Affiche les articulations
            foreach (Joint joint in skeleton.Joints)
            {
                Brush drawBrush = null;

                if (joint.JointType.Equals(JointType.Head))
                {
                    Console.WriteLine("Tête présente");
                    if (joint.TrackingState == JointTrackingState.Tracked)
                    {
                        Console.WriteLine("Tête trackée");
                        drawBrush = this.trackedJointBrush;
                    }
                    else if (joint.TrackingState == JointTrackingState.Inferred)
                    {
                        Console.WriteLine("Tête devinée");
                        drawBrush = this.inferredJointBrush;
                    }
                    headX = joint.Position.X;
                    headY = joint.Position.Y;
                    headZ = joint.Position.Z;
                }

                if (drawBrush != null)
                {
                    Console.WriteLine("Dessin des points");
                    drawingContext.DrawEllipse(drawBrush, null, this.SkeletonPointToScreen(joint.Position), JointThickness, JointThickness);
                }
            }
        }

        // Convertit les coordonnées des points d'un squelette en coordonnées de points affichables à l'écran
        private Point SkeletonPointToScreen(SkeletonPoint skelpoint)
        {
            DepthImagePoint depthPoint = this.sensor.CoordinateMapper.MapSkeletonPointToDepthPoint(skelpoint, DepthImageFormat.Resolution640x480Fps30);
            return new Point(depthPoint.X, depthPoint.Y);
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {

            this.BoutonRetour.Background = utils.red();
            this.BoutonRetour.Foreground = utils.white();
            this.BoutonStart.Background = utils.green();
            this.BoutonStart.Foreground = utils.white();
            this.BoutonStop.Background = utils.green();
            this.BoutonStop.Foreground = utils.white();

            for (int i = 10; i >= 0; i--)
            {
                var button = new KinectCircleButton
                {
                    Content = i,
                    Height = 128,
                    Width = 100,
                    Background = utils.yellow(),
                    Foreground = utils.white()
                };

                float i1 = i;
                // Envoie le volume en pourcentages
                button.Click +=
                    (o, args) =>
                        osc_sendVolume(i1 / 10);

                liste.Children.Add(button);
            }

        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.sensorChooser.Stop();
        }

        private static void SensorChooserOnKinectChanged(object sender, KinectChangedEventArgs args)
        {
            if (args.OldSensor != null)
            {
                try
                {
                    args.OldSensor.DepthStream.Range = DepthRange.Near;
                    args.OldSensor.DepthStream.Disable();
                }
                catch (InvalidOperationException)
                {
                }
            }

            if (args.NewSensor != null)
            {
                try
                {
                    args.NewSensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
                    args.NewSensor.SkeletonStream.Enable();

                    try
                    {
                        args.NewSensor.DepthStream.Range = DepthRange.Default;
                        args.NewSensor.SkeletonStream.EnableTrackingInNearRange = true;
                    }
                    catch (InvalidOperationException)
                    {
                        args.NewSensor.DepthStream.Range = DepthRange.Default;
                        args.NewSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    }
                }
                catch (InvalidOperationException)
                {
                }
            }
        }

        // Définition du bouton X
        private void KinectTileButton_Click_retour(object sender, RoutedEventArgs e)
        {
            if (stop == 0)
                osc_sendStop();
            this.sensorChooser.KinectChanged -= SensorChooserOnKinectChanged;
            (Application.Current.MainWindow.FindName("_mainFrame") as Frame).Source = new Uri("PageMenu.xaml", UriKind.Relative);

        }

        // Définition du bouton start
        private void KinectTileButton_Click_start(object sender, RoutedEventArgs e)
        {
            if (stop == 1)
            {
                osc_sendStart();
                stop = 0;
            }

        }

        // Définition du bouton stop
        private void KinectTileButton_Click_stop(object sender, RoutedEventArgs e)
        {
            if (stop == 0)
            {
                osc_sendStop();
                stop = 1;
            }
        }

        // Permet de calculer la distance entre la tête de l'utilisateur et l'objet qu'il tient
        private void DistanceHeadObject(Skeleton s, Spherique sph)
        {
        // Initialisation des variables à 50000, valeur qu'on ne pourra jamais atteindre avec la Kinect
            float hx = 50000, hy = 50000, hz = 50000, ox = 50000, oy = 50000, oz = 50000, px, py, pz;
            foreach (Joint joint in s.Joints)
            {
                if (joint.JointType.Equals(JointType.Head))
                {
                    hx = joint.Position.X;
                    hy = joint.Position.Y;
                    hz = joint.Position.Z;
                }
                if (joint.JointType.Equals(JointType.HandRight))
                {
                    ox = joint.Position.X;
                    oy = joint.Position.Y;
                    oz = joint.Position.Z;
                }
                if (hx != 50000 && ox != 50000)
                {
                    px = ox - hx;
                    py = oy - hy;
                    pz = oz - hz;
                    // Conversion des données dans le système sphérique
                    sph.update(px, py, pz);
                }

            }


        }



    }

}
