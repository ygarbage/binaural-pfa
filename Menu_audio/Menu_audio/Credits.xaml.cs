﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Menu_audio
{
    /// <summary>
    /// Logique d'interaction pour Credits.xaml
    /// </summary>
    public partial class Credits : Page
    {
        private Utils utils = new Utils();
        public Credits()
        {
            InitializeComponent();
 
            this.BoutonRetour.Background = utils.red();
            this.BoutonRetour.Foreground = utils.white();
            Bloc.FontSize = 36;
            Bloc.Text = "Ce projet a été réalisé par une équipe de l'ENSEIRB-MATMECA, composée de :\nGéraldine Alié\nClément Dussieux\nYvon Garbage\nGaëtan Le Bris\nRémi Millet\nRomain Scimia\net Guillaume Vandeneeckhoutte,\net encadrée par :\nMme Myriam Desainte-Catherine,\ndans le cadre des PFA.";
        }
            private void KinectTileButton_Click_retour(object sender, RoutedEventArgs e)
        {
            (Application.Current.MainWindow.FindName("_mainFrame") as Frame).Source = new Uri("PageAccueil.xaml", UriKind.Relative);

        }
        



    }
}
