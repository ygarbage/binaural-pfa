﻿using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Menu_audio
{
    /// <summary>
    /// Logique d'interaction pour MenuHRTF.xaml
    /// </summary>
    public partial class MenuHRTF : Page
    {

        private Utils utils = new Utils();

        /* Chargement dynamique des dll du moduleOSC */
        [DllImport("Envoi_osc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int osc_sendLoad(String audio);

        [DllImport("Envoi_osc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int osc_sendHRTFOpt(String htrf);
        /* Fin du chargement dynamique */

        private readonly KinectSensorChooser sensorChooser;

        public MenuHRTF()
        {
            // Initialisation de la Kinect
            this.InitializeComponent();
            this.sensorChooser = new KinectSensorChooser();
            // Réutilisation du sensorChooser de la première fenêtre
            this.sensorChooser = Generics.GlobalKinectSensorChooser;
            this.sensorChooser.KinectChanged += SensorChooserOnKinectChanged;
            this.sensorChooserUi.KinectSensorChooser = sensorChooser;
            // On relie le capteur Kinect à la KinectRegion définie dans le .xaml
            var regionSensorBinding = new Binding("Kinect") { Source = this.sensorChooser };
            BindingOperations.SetBinding(this.kinectRegion, KinectRegion.KinectSensorProperty, regionSensorBinding);

            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {

            // Remplissage de la liste des HRTF
            for (int i = 1; i < 20; i++)
            {
                var button = new KinectTileButton
                {
                    Content = "HRTF " + i,
                    Height = 128,
                    Width = 800,
                    Background = utils.blue(),
                    Foreground = utils.white()
                };

                int i1 = i;
               
                button.Click +=
                    (o, args) =>
                        osc_sendHRTFOpt("HRTF" + i1);

                button.Click +=
                    (o, args) =>
                        (Application.Current.MainWindow.FindName("_mainFrame") as Frame).Source = new Uri("PageMenu.xaml", UriKind.Relative); 
                
                liste.Children.Add(button);
            }
        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.sensorChooser.Stop();
        }

        private static void SensorChooserOnKinectChanged(object sender, KinectChangedEventArgs args)
        {
            if (args.OldSensor != null)
            {
                try
                {
                    args.OldSensor.DepthStream.Range = DepthRange.Near;
                    args.OldSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    args.OldSensor.DepthStream.Disable();
                    args.OldSensor.SkeletonStream.Disable();
                }
                catch (InvalidOperationException)
                {
                }
            }

            if (args.NewSensor != null)
            {
                try
                {
                    args.NewSensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
                    args.NewSensor.SkeletonStream.Enable();

                    try
                    {
                        args.NewSensor.DepthStream.Range = DepthRange.Default;
                        args.NewSensor.SkeletonStream.EnableTrackingInNearRange = true;
                    }
                    catch (InvalidOperationException)
                    {
                        args.NewSensor.DepthStream.Range = DepthRange.Default;
                        args.NewSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    }
                }
                catch (InvalidOperationException)
                {
                }
            }
        }

    }
}
