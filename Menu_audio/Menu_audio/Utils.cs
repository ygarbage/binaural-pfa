﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Menu_audio
{
    

    class Utils
    {

        // Classe permettant d'appliquer une couleur sur les boutons du menu

        private System.Windows.Media.BrushConverter converter = new System.Windows.Media.BrushConverter();

        public Brush red()
        {
            return (Brush)converter.ConvertFromString("#d50f25");
        }

         public Brush yellow()
        {
            return (Brush)converter.ConvertFromString("#eeb211");
        }

         public Brush blue()
         {
             return (Brush)converter.ConvertFromString("#3369e8");
         }

         public Brush green()
         {
             return (Brush)converter.ConvertFromString("#009939");
         }

        public Brush white()
        {
            return (Brush)converter.ConvertFromString("#ffffff");
        }

        public Brush black()
        {
            return (Brush)converter.ConvertFromString("#000000");
        }

    }
}
