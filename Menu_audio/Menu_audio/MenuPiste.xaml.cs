﻿using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.Controls;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Menu_audio
{
    /// <summary>
    /// Logique d'interaction pour MenuPiste.xaml
    /// </summary>
    public partial class MenuPiste : Page
    {

        private Utils utils = new Utils();

        /* Chargement dynamique des dll du module OSC */
        [DllImport("Envoi_osc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int osc_sendLoad(String audio);
        /* Fin du chargement dynamique */

        private const String trackFolder = @"..\..\..\..\sons";

        private readonly KinectSensorChooser sensorChooser;

        public MenuPiste()
        { 
            // Initialisation de la Kinect
            this.InitializeComponent();
            this.sensorChooser = new KinectSensorChooser();
            // Réutilisation du sensorChooser de la première fenêtre
            this.sensorChooser = Generics.GlobalKinectSensorChooser;
            this.sensorChooser.KinectChanged += SensorChooserOnKinectChanged;
            this.sensorChooserUi.KinectSensorChooser = sensorChooser;
            // On relie le capteur Kinect à la KinectRegion définie dans le .xaml
            var regionSensorBinding = new Binding("Kinect") { Source = this.sensorChooser };
            BindingOperations.SetBinding(this.kinectRegion, KinectRegion.KinectSensorProperty, regionSensorBinding);

            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {

            // Remplissage de la liste des pistes audio
            if (Directory.Exists(trackFolder)) { // Si le dossier existe, lister les fichiers et les ajouter dans la liste
                string[] fichiers = Directory.GetFiles(trackFolder, "*.wav", SearchOption.AllDirectories);
                foreach (string fichier in fichiers)
                {
                    Console.WriteLine("Fichier : {0}\n", fichier);

                    var button = new KinectTileButton {
                        Content = fichier.Substring(fichier.LastIndexOf('\\') + 1),
                        Height = 128,
                        Width = 800,
                        FontFamily = new FontFamily("Bradley Hand ITC"),
                        Background = utils.red(),
                        Foreground = utils.white()
                    };

                    button.Click +=
                        (o, args) =>
                            osc_sendLoad(fichier.Substring(fichier.LastIndexOf('\\') + 1));

                    button.Click +=
                        (o, args) =>
                            (Application.Current.MainWindow.FindName("_mainFrame") as Frame).Source = new Uri("PageMenu.xaml", UriKind.Relative);

                    liste.Children.Add(button);
                }
            }
            else { // Sinon, afficher un message d'erreur
                var button = new KinectTileButton
                {
                    Content = "Erreur : aucun son trouvé !",
                    Height = 128,
                    Width = 800
                };

                // Retour vers l'écran précédent si on clique pour éviter la frustration
                button.Click +=
                    (o, args) =>
                        (Application.Current.MainWindow.FindName("_mainFrame") as Frame).Source = new Uri("PageMenu.xaml", UriKind.Relative);

                liste.Children.Add(button);

                var buttonRetour = new KinectTileButton
                {
                    Content = "Retour",
                    Height = 128,
                    Width = 800
                };

                buttonRetour.Click +=
                   (o, args) =>
                       (Application.Current.MainWindow.FindName("_mainFrame") as Frame).Source = new Uri("PageMenu.xaml", UriKind.Relative);

                liste.Children.Add(buttonRetour);
            }
            
        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.sensorChooser.Stop();
        }

        private static void SensorChooserOnKinectChanged(object sender, KinectChangedEventArgs args)
        {
            if (args.OldSensor != null)
            {
                try
                {
                    args.OldSensor.DepthStream.Range = DepthRange.Near;
                    args.OldSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    args.OldSensor.DepthStream.Disable();
                    args.OldSensor.SkeletonStream.Disable();
                }
                catch (InvalidOperationException)
                {
                }
            }

            if (args.NewSensor != null)
            {
                try
                {
                    args.NewSensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
                    args.NewSensor.SkeletonStream.Enable();

                    try
                    {
                        args.NewSensor.DepthStream.Range = DepthRange.Default;
                        args.NewSensor.SkeletonStream.EnableTrackingInNearRange = true;
                    }
                    catch (InvalidOperationException)
                    {
                        args.NewSensor.DepthStream.Range = DepthRange.Default;
                        args.NewSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    }
                }
                catch (InvalidOperationException)
                {
                }
            }
        }

    }
}
