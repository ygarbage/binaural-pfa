﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Menu_audio
{
    class Spherique
    {
        public float lon, lat, prof;

        // Permet de convertir des données cartésiennes en données sphériques, pour être exploitables par PureData
        public void update(float x, float y, float z)
        {
            prof = (float)Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2) + Math.Pow(z, 2));
            lat = 180 * (float)Math.Atan(y / Math.Sqrt(Math.Pow(x, 2) + Math.Pow(z, 2))) / (float)Math.PI;

            lon = 180 * (float)Math.Acos(x / Math.Sqrt(Math.Pow(x, 2) + Math.Pow(z, 2))) / (float)Math.PI;
            if (z > 0)
                lon = 360 - lon;
            lon = lon - 90;
            if (lon < 0)
                lon = 360 + lon;
        }

        public Spherique(){
            prof = 0;
            lon = 0;
            lat = 0;
        }
    }
}
