﻿using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.Controls;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Menu_audio
{
    /// <summary>
    /// Logique d'interaction pour PageAccueil.xaml
    /// </summary>
    public partial class PageAccueil : Page
    {
        /* Chargement dynamique des dll du module OSC */
        [DllImport("Envoi_osc.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int osc_sendLoad(String audio);
        /* Fin du chargement dynamique */

        private Utils utils = new Utils();
        private readonly KinectSensorChooser sensorChooser;

        public PageAccueil()
        {
            try
            {
                this.InitializeComponent();
            }
            catch (Exception ex)
            {

            }
            if (Generics.GlobalKinectSensorChooser == null)
            {
                // Initialisation de la Kinect
                this.sensorChooser = new KinectSensorChooser();
                this.sensorChooser.KinectChanged += SensorChooserOnKinectChanged;
                this.sensorChooserUi.KinectSensorChooser = this.sensorChooser;
                this.sensorChooser.Start();
                Generics.GlobalKinectSensorChooser = this.sensorChooser;
            }
            else
            {   
                // Initialisation de la Kinect 
                this.sensorChooser = new KinectSensorChooser();
                this.sensorChooser = Generics.GlobalKinectSensorChooser;
                this.sensorChooser.KinectChanged += SensorChooserOnKinectChanged;
                this.sensorChooserUi.KinectSensorChooser = sensorChooser;
            }
            // On relie le capteur Kinect à la KinectRegion définie dans le .xaml
            var regionSensorBinding = new Binding("Kinect") { Source = this.sensorChooser };
            BindingOperations.SetBinding(this.kinectRegion, KinectRegion.KinectSensorProperty, regionSensorBinding);

            // On sélectionne une piste par défaut
            osc_sendLoad("piano.wav");

            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            this.BoutonDemarrer.Background = utils.red();
            this.BoutonDemarrer.Foreground = utils.white();
            this.BoutonRetour.Background = utils.red();
            this.BoutonRetour.Foreground = utils.white();
            this.BoutonCredits.Background = utils.red();
            this.BoutonCredits.Foreground = utils.white();
        }

        private static void SensorChooserOnKinectChanged(object sender, KinectChangedEventArgs args)
        {
            if (args.OldSensor != null)
            {
                try
                {
                    args.OldSensor.DepthStream.Range = DepthRange.Near;
                    args.OldSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    args.OldSensor.DepthStream.Disable();
                    args.OldSensor.SkeletonStream.Disable();
                }
                catch (InvalidOperationException)
                {
                }
            }

            if (args.NewSensor != null)
            {
                try
                {
                    args.NewSensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
                    args.NewSensor.SkeletonStream.Enable();

                    try
                    {
                        args.NewSensor.DepthStream.Range = DepthRange.Default;
                        args.NewSensor.SkeletonStream.EnableTrackingInNearRange = true;
                    }
                    catch (InvalidOperationException)
                    {
                        args.NewSensor.DepthStream.Range = DepthRange.Default;
                        args.NewSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    }
                }
                catch (InvalidOperationException)
                {
                }
            }
        }

        // Définition du bouton Démarrer
        private void KinectTileButton_Click_demarrer(object sender, RoutedEventArgs e)
        {
            this.sensorChooser.KinectChanged -= SensorChooserOnKinectChanged;
            (Application.Current.MainWindow.FindName("_mainFrame") as Frame).Source = new Uri("PageMenu.xaml", UriKind.Relative);
        }

        // Définition du bouton Crédits
        private void KinectTileButton_Click_credits(object sender, RoutedEventArgs e)
        {
            this.sensorChooser.KinectChanged -= SensorChooserOnKinectChanged;
            (Application.Current.MainWindow.FindName("_mainFrame") as Frame).Source = new Uri("Credits.xaml", UriKind.Relative);
        }

        // Définition du bouton X
        private void KinectTileButton_Click_retour(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
