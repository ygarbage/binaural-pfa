#include <opencv/cvaux.h>
#include <opencv/highgui.h>
#include <opencv/cxcore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <iostream>
#include <float.h>
#include <limits.h>
#include <time.h>
#include <ctype.h>
#include <Windows.h>
#include "NuiApi.h"


#define COLOR_WIDTH 640    
#define COLOR_HIGHT 480    
#define DEPTH_WIDTH 320    
#define DEPTH_HIGHT 240    
#define SKELETON_WIDTH 640    
#define SKELETON_HIGHT 480    
#define CHANNEL 3

//Default capture size - 640x480
CvSize size = cvSize(640, 480);
USHORT x, y, z = 0;

BYTE buf[DEPTH_WIDTH * DEPTH_HIGHT * CHANNEL];
using namespace std;
using namespace cv;


//Affiche l'image en couleur avec les cercles autour des formes rondes d�t�ct�es
int drawColor(HANDLE h, IplImage* frame)
{
	//On r�cup�re l'image de Kinect et on la met dans une IplImageFrame (OPENCV)
	const NUI_IMAGE_FRAME * pImageFrame = NULL;
	HRESULT hr = NuiImageStreamGetNextFrame(h, 0, &pImageFrame);
	if (FAILED(hr))
	{
		cout << "Get Image Frame Failed" << endl;
		return -1;
	}
	INuiFrameTexture * pTexture = pImageFrame->pFrameTexture;
	NUI_LOCKED_RECT LockedRect;
	pTexture->LockRect(0, &LockedRect, NULL, 0);
	if (LockedRect.Pitch != 0)
	{
		BYTE * pBuffer = (BYTE*)LockedRect.pBits;
		cvSetData(frame, pBuffer, LockedRect.Pitch);
	}


	//-------Detection Objets OPENCV--------------

	//Detection teintes
	CvScalar hsv_min = CV_RGB(145, 145, 10);
	CvScalar hsv_max = CV_RGB(255, 255, 128);

	IplImage *  hsv_frame = cvCreateImage(size, IPL_DEPTH_8U, 3);
	IplImage*  thresholded = cvCreateImage(size, IPL_DEPTH_8U, 1);

	//Converti en HSV pour que ce soit plus simple de detecter les teintes
	cvCvtColor(frame, hsv_frame, CV_RGB2HSV);
	//On enleve les couleurs qui ne sont pas bonnes
	cvInRangeS(hsv_frame, hsv_min, hsv_max, thresholded);
	// Memory for hough circles
	CvMemStorage* storage = cvCreateMemStorage(0);
	//On adouci l'image pour la detection des cercles
	cvSmooth(thresholded, thresholded, CV_GAUSSIAN, 9, 9);
	CvSeq* circles = cvHoughCircles(thresholded, storage, CV_HOUGH_GRADIENT, 2,
		thresholded->height / 4, 100, 50, 10, 400);
	for (int i = 0; i < circles->total; i++)
	{
		float* p = (float*)cvGetSeqElem(circles, i);
		printf("Ball! x=%f y=%f r=%f\n\r", p[0], p[1], p[2]); //rayon et x et y de la balle
		//on les sauvegarde dans les variables x y globales
		x = (USHORT) p[0];
		y = (USHORT) p[1];
		//Ca c'est juste pour dessiner un rond rouge autour des balles
		cvCircle(frame, cvPoint(cvRound(p[0]), cvRound(p[1])),
			3, CV_RGB(0, 255, 0), -1, 8, 0);
		cvCircle(frame, cvPoint(cvRound(p[0]), cvRound(p[1])),
			cvRound(p[2]), CV_RGB(255, 0, 0), 3, 8, 0);
	}
	cvShowImage("color image", frame);
	//delete(storage);
	NuiImageStreamReleaseFrame(h, pImageFrame);
	return 0;
}

int drawDepth(HANDLE h, IplImage* depth)
{
	const NUI_IMAGE_FRAME * pImageFrame = NULL;
	HRESULT hr = NuiImageStreamGetNextFrame(h, 0, &pImageFrame);
	if (FAILED(hr))
	{
		cout << "Get Image Frame Failed" << endl;
		return -1;
	}
	//  temp1 = depth;
	//distance = GetDistance((byte)pixelData_Depth[i], (byte)pixelData_Depth[i + 1]);
	INuiFrameTexture * pTexture = pImageFrame->pFrameTexture;
	NUI_LOCKED_RECT LockedRect;
	pTexture->LockRect(0, &LockedRect, NULL, 0);
	if (LockedRect.Pitch != 0)
	{
		USHORT * pBuff = (USHORT*)LockedRect.pBits;
		USHORT coords = ((USHORT)x / 2) + (((USHORT)y / 2) * DEPTH_WIDTH); //on stocke x et y dans coords
		USHORT realDepth = (pBuff[coords] & 0xFFF8) >> 3;
		//USHORT realDepth = pBuff[coords];
		z = realDepth;  //on sauvegarde la profondeur du pixel coords


		//Juste pour afficher une image de profondeur
		//A commenter si on veut pas la voir
		// + commenter cvshowimage
		for (int i = 0; i < DEPTH_WIDTH * DEPTH_HIGHT; i++)
		{
			BYTE index = pBuff[i] & 0x07;
			USHORT realDepth = (pBuff[i] & 0xFFF8) >> 3;

			BYTE scale = 255 - (BYTE)(256 * realDepth / 0x0fff);
			buf[CHANNEL * i] = buf[CHANNEL * i + 1] = buf[CHANNEL * i + 2] = 0;
			switch (index)
			{
			case 0:
				buf[CHANNEL * i] = scale / 2;
				buf[CHANNEL * i + 1] = scale / 2;
				buf[CHANNEL * i + 2] = scale / 2;
				break;
			case 1:
				buf[CHANNEL * i] = scale;
				break;
			case 2:
				buf[CHANNEL * i + 1] = scale;
				break;
			case 3:
				buf[CHANNEL * i + 2] = scale;
				break;
			case 4:
				buf[CHANNEL * i] = scale;
				buf[CHANNEL * i + 1] = scale;
				break;
			case 5:
				buf[CHANNEL * i] = scale;
				buf[CHANNEL * i + 2] = scale;
				break;
			case 6:
				buf[CHANNEL * i + 1] = scale;
				buf[CHANNEL * i + 2] = scale;
				break;
			case 7:
				buf[CHANNEL * i] = 255 - scale / 2;
				buf[CHANNEL * i + 1] = 255 - scale / 2;
				buf[CHANNEL * i + 2] = 255 - scale / 2;
				break;
			}
		}
		cvSetData(depth, buf, DEPTH_WIDTH * CHANNEL);
	}
	NuiImageStreamReleaseFrame(h, pImageFrame);
	cvShowImage("depth image", depth);//On affiche l'image de profondeur a commenter si on en veut pas
	return 0;
}


int main(int argc, char * argv[])
{

	IplImage* color = cvCreateImageHeader(cvSize(COLOR_WIDTH, COLOR_HIGHT), IPL_DEPTH_8U, 4);

	IplImage* depth = cvCreateImageHeader(cvSize(DEPTH_WIDTH, DEPTH_HIGHT), IPL_DEPTH_8U, CHANNEL);


	//Fenetre d'affichage
	cvNamedWindow("color image", CV_WINDOW_AUTOSIZE);

	cvNamedWindow("depth image", CV_WINDOW_AUTOSIZE);


	HRESULT hr = NuiInitialize(
		NUI_INITIALIZE_FLAG_USES_DEPTH_AND_PLAYER_INDEX
		| NUI_INITIALIZE_FLAG_USES_COLOR
		| NUI_INITIALIZE_FLAG_USES_SKELETON);

	if (hr != S_OK)
	{
		cout << "NuiInitialize failed" << endl;
		return hr;
	}

	HANDLE h1 = CreateEvent(NULL, TRUE, FALSE, NULL);
	HANDLE h2 = NULL;
	hr = NuiImageStreamOpen(NUI_IMAGE_TYPE_COLOR, NUI_IMAGE_RESOLUTION_640x480,
		0, 2, h1, &h2);
	if (FAILED(hr))
	{
		cout << "Could not open image stream video" << endl;
		return hr;
	}

	HANDLE h3 = CreateEvent(NULL, TRUE, FALSE, NULL);
	HANDLE h4 = NULL;
	hr = NuiImageStreamOpen(NUI_IMAGE_TYPE_DEPTH_AND_PLAYER_INDEX,
	NUI_IMAGE_RESOLUTION_320x240, 0, 2, h3, &h4);
	if (FAILED(hr))
	{
	cout << "Could not open depth stream video" << endl;
	return hr;
	}



	while (1)
	{
		WaitForSingleObject(h1, INFINITE);
		drawColor(h2, color);//on detecte la balle
		WaitForSingleObject(h3, INFINITE);
		drawDepth(h4, depth);//On detecte sa profondeur


		//exit
		int c = cvWaitKey(1);
		if (c == 27 || c == 'q' || c == 'Q')
			break;
	}

	cvReleaseImageHeader(&depth);
	cvReleaseImageHeader(&color);

	cvDestroyWindow("depth image");
	cvDestroyWindow("color image");


	NuiShutdown();

	return 0;

}